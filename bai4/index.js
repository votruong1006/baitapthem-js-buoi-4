function tinhToaDo() {
  var tenSV1 = document.getElementById("ten1").value;
  var tenSV2 = document.getElementById("ten2").value;
  var tenSV3 = document.getElementById("ten3").value;
  var x1 = document.getElementById("x1").value * 1;
  var y1 = document.getElementById("y1").value * 1;
  var x2 = document.getElementById("x2").value * 1;
  var y2 = document.getElementById("y2").value * 1;
  var x3 = document.getElementById("x3").value * 1;
  var y3 = document.getElementById("y3").value * 1;
  var xt = document.getElementById("xt").value * 1;
  var yt = document.getElementById("yt").value * 1;
  var d1, d2, d3, result;

  d1 = Math.sqrt((x1 - xt) ** 2 + (y1 - yt) ** 2);
  d2 = Math.sqrt((x2 - xt) ** 2 + (y2 - yt) ** 2);
  d3 = Math.sqrt((x3 - xt) ** 2 + (y3 - yt) ** 2);

  if (tenSV1 == "" || tenSV2 == "" || tenSV2 == "") {
    document.getElementById(
      "result-b4"
    ).innerHTML = `Dữ liệu không hợp lệ (thiếu tên sinh viên)`;
  } else {
    if (d1 > d2 && d1 > d3) {
      result = tenSV1;
    } else if (d2 > d1 && d2 > d3) result = tenSV2;
    else {
      result = tenSV3;
    }
    document.getElementById(
      "result-b4"
    ).innerHTML = `Sinh viên xa trường nhất: ${result}`;
  }
}
/**
 * Input: nhập tên và tọa độ của sinh viên, trường
 *
 * Các bước thực hiện:
 *    Bước1: Khai báo và gán biến tenSV1, tenSV2, tenSV3, x1, y1, x2, y2, x3, y3 theo giá trị của HTML element
 *    Bước2: Khai báo biến d1, d2, d3, result
 *    Bước3: Gán biến d1 = Math.sqrt((x1 - xt) ** 2 + (y1 - yt) ** 2)
 *    Bước4: Gán biến d2 = Math.sqrt((x2 - xt) ** 2 + (y2 - yt) ** 2)
 *    Bước5: Gán biến d3 = Math.sqrt((x3 - xt) ** 2 + (y3 - yt) ** 2)
 *    Bước6: Dùng cấu trúc if-else:
 *      Nếu giá trị tên sinh viên bị rỗng thì in dòng chữ "Dữ liệu không hợp lệ (thiếu tên sinh viên)"
 *      Nếu không thì :
 *          Nếu d1 > d2 && d1 > d3 thì result = tenSV1
 *          Nếu d2 > d1 && d2 > d3 thì result = tenSV2
 *          Trường hợp còn lại result = tenSV3
 *    Bước7: In kết quả ra giao diện web
 *
 * Output: cho biết sinh viên nào xa trường nhất
 */
