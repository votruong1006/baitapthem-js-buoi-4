function tinhNgay() {
  var month = document.getElementById("thang").value * 1;
  var year = document.getElementById("nam").value * 1;
  if (year > 1900 && month >= 1 && month <= 12) {
    if (month == 4 || month == 6 || month == 9 || month == 11) {
      document.getElementById(
        "result"
      ).innerText = `Tháng ${month} năm ${year} có 30 ngày`;
    } else if (month == 2) {
      if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        document.getElementById(
          "result"
        ).innerText = `Tháng ${month} năm ${year} có 29 ngày`;
      } else {
        document.getElementById(
          "result"
        ).innerText = `Tháng ${month} năm ${year} có 28 ngày`;
      }
    } else {
      document.getElementById(
        "result"
      ).innerText = `Tháng ${month} năm ${year} có 31 ngày`;
    }
  } else {
    document.getElementById(
      "result"
    ).innerText = `Năm cần lớn hơn 1900 và tháng hợp lí . Xin nhập lại`;
  }
}
//input: tháng và năm mà người dùng nhập
//các bước xử lý:
//  b1:tạo 2 biến month và year ứng với tháng và năm người dùng đã nhập
//  b2:gán giá trị cho month và year
//  b3:dùng câu điều kiện if-else để giải quyết.trong if ta cho đk là year > 1900 && month >= 1 && month <= 12 để xét tháng và năm là hợp lệ
//  b4:nếu hợp lệ ta xét
//     Đối với các tháng 1,3,5,7,8,10,12 sẽ có 31 ngày ta in ra màn hình `Tháng ${month} năm ${year} có 31 ngày`
//     Đối với các tháng 4,6,9,11 sẽ có 30 ngày ta in ra màn hình `Tháng ${month} năm ${year} có 30 ngày`
//     Đối với tháng 2, nếu là năm nhuận sẽ có 29 ngày ta in ra màn hình `Tháng ${month} năm ${year} có 29 ngày`
//     Ngược lại là 28 ngày ta in ra màn hình `Tháng ${month} năm ${year} có 28 ngày`.
//     Cách kiểm tra năm nhuận:
//     Những năm chia hết cho 4 và không chia hết cho 100 ,hoặc những năm chia hết cho 400
//  b5:nếu ko hợp lệ ta in ra màn hình `Năm cần lớn hơn 1900 và tháng hợp lí . Xin nhập lại`
//output: số ngày trong tháng và năm nguoif dùng vừa nhập
