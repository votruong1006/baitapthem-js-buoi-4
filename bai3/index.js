function docSo() {
  var number = document.getElementById("so").value * 1;
  var hangTram = Math.floor(number / 100);
  var hangChuc = Math.floor((number % 100) / 10);
  var hangDV = number % 10;
  // switch ((hangTram, hangChuc, hangDV)) {
  //   case 0:
  //     return (document.getElementById("result").innerText = `Không hợp lệ`);

  //   case 1:
  //     hangTram = "Một";
  //     hangChuc = "Mười";
  //     hangDV = "Một";
  //     return (document.getElementById(
  //       "result"
  //     ).innerText = `${hangTram} trăm ${hangChuc} ${hangDV}`);
  //     break;
  //   case 2:
  //     hangTram = "Hai";
  //     hangChuc = "Hai";
  //     hangDV = "Hai";
  //     break;
  //   case 3:
  //     hangTram = "Ba";
  //     hangChuc = "Ba";
  //     hangDV = "Ba";
  //     break;
  //   case 4:
  //     hangTram = "Bốn";
  //     hangChuc = "Bốn";
  //     hangDV = "Bốn";
  //     break;
  //   case 5:
  //     hangTram = "Năm";
  //     hangChuc = "Năm";
  //     hangDV = "Lăm";
  //     break;
  //   case 6:
  //     hangTram = "Sáu";
  //     hangChuc = "Sáu";
  //     hangDV = "Sáu";
  //     break;
  //   case 7:
  //     hangTram = "Bảy";
  //     hangChuc = "Bảy";
  //     hangDV = "Bảy";
  //     break;
  //   case 8:
  //     hangTram = "Tám";
  //     hangChuc = "Tám";
  //     hangDV = "Tám";
  //     break;
  //   case 9:
  //     hangTram = "Chín";
  //     hangChuc = "Chín";
  //     hangDV = "Chín";
  //     break;

  //   default:
  //     break;
  // }
  // if (hangTram == 0) {
  //   return (document.getElementById("result").innerText = `Không hợp lệ`);
  // } else if (hangChuc == 0 && hangDV == 0) {
  //   return (document.getElementById("result").innerText = `${hangTram} Trăm `);
  // }
  switch (hangTram) {
    case 1:
      hangTram = "Một";
      break;
    case 2:
      hangTram = "Hai";
      break;
    case 3:
      hangTram = "Ba";
      break;
    case 4:
      hangTram = "Bốn";
      break;
    case 5:
      hangTram = "Năm";
      break;
    case 6:
      hangTram = "Sáu";
      break;
    case 7:
      hangTram = "Bảy";
      break;
    case 8:
      hangTram = "Tám";
      break;
    case 9:
      hangTram = "Chín";
      break;
    default:
      break;
  }
  switch (hangChuc) {
    case 2:
      hangChuc = "Hai";
      break;
    case 3:
      hangChuc = "Ba";
      break;
    case 4:
      hangChuc = "Bốn";
      break;
    case 5:
      hangChuc = "Năm";
      break;
    case 6:
      hangChuc = "Sáu";
      break;
    case 7:
      hangChuc = "Bảy";
      break;
    case 8:
      hangChuc = "Tám";
      break;
    case 9:
      hangChuc = "Chín";
      break;
    default:
      break;
  }
  switch (hangDV) {
    case 1:
      hangDV = "Một";
      break;
    case 2:
      hangDV = "Hai";
      break;
    case 3:
      hangDV = "Ba";
      break;
    case 4:
      hangDV = "Bốn";
      break;
    case 5:
      hangDV = "Năm";
      break;
    case 6:
      hangDV = "Sáu";
      break;
    case 7:
      hangDV = "Bảy";
      break;
    case 8:
      hangDV = "Tám";
      break;
    case 9:
      hangDV = "Chín";
      break;
    default:
      break;
  }
  if (hangTram == 0) {
    return (document.getElementById("result").innerText = `Không hợp lệ`);
  } else if (hangChuc == 0 && hangDV == 0) {
    document.getElementById("result").innerText = `${hangTram} Trăm `;
  } else if (hangChuc == 1 && hangDV == 0) {
    document.getElementById("result").innerText = `${hangTram} Trăm Mười`;
  } else if (hangChuc == 0 && hangDV != 0) {
    document.getElementById(
      "result"
    ).innerText = `${hangTram} Trăm Lẻ ${hangDV}`;
  } else if (hangChuc == 1 && hangDV != 0) {
    document.getElementById(
      "result"
    ).innerText = `${hangTram} Trăm Mười ${hangDV}`;
  } else {
    document.getElementById(
      "result"
    ).innerText = `${hangTram} Trăm ${hangChuc} Mươi ${hangDV} `;
  }
}

/**
 * Input: số tự nhiên có 3 chữ số
 *
 * Các bước thực hiện:
 *    Bước1: Khai báo và gán biến number theo giá trị của HTML element
 *    Bước2: Khai báo biến hangTram, hangChuc, hangDV, chuHangTram, chuHangChuc, chuhangDV
 *    Bước3: Gán biến hangTram = Math.floor(n / 100)
 *    Bước4: Gán biến hangChuc = Math.floor(n % 100 /10)
 *    Bước5: Gán biến hangDV = Math.floor(n % 10)
 *    Bước6: Dùng cấu trúc swinch-case để gán tên cho giá trị của hangTram, hangChuc, hangDV
 *    Bước7: dùng cấu trúcif-else-if để xét các kết quả phù hợp và In kết quả ra giao diện web
 *
 *
 * Output: cách đọc số vừa nhập
 */
