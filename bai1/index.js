function tinhNgayTruoc() {
  var date = document.getElementById("date").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var ngayHomQua, thangTruoc, namTruoc;
  if (date < 1 || date > 31 || month < 1 || month > 12 || year <= 0) {
    document.getElementById("result-b1").innerText = `Dữ liệu không hợp lệ`;
  } else {
    if (date == 1) {
      if (month == 5 || month == 7 || month == 10 || month == 12) {
        ngayHomQua = 30;
        thangTruoc = month - 1;
        namTruoc = year;
      } else if (
        month == 2 ||
        month == 4 ||
        month == 6 ||
        month == 8 ||
        month == 9 ||
        month == 11
      ) {
        ngayHomQua = 31;
        thangTruoc = month - 1;
        namTruoc = year;
      } else if (month == 1) {
        ngayHomQua = 31;
        thangTruoc = 12;
        namTruoc = year - 1;
      } else {
        if (year % 4 == 0 && year % 100 != 0) {
          ngayHomQua = 29;
          thangTruoc = month - 1;
          namTruoc = year;
        } else {
          ngayHomQua = 28;
          thangTruoc = month - 1;
          namTruoc = year;
        }
      }
    } else {
      ngayHomQua = date - 1;
      thangTruoc = month;
      namTruoc = year;
    }
    document.getElementById(
      "result-b1"
    ).innerText = `${ngayHomQua}/${thangTruoc}/${namTruoc}`;
  }
}

function tinhNgaySau() {
  var date = document.getElementById("date").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var ngayMai, thangSau, namSau;
  if (date < 1 || date > 31 || month < 1 || month > 12 || year <= 0) {
    document.getElementById("result-b1").innerText = `Dữ liệu không hợp lệ`;
  } else {
    if (
      month == 1 ||
      month == 3 ||
      month == 5 ||
      month == 7 ||
      month == 8 ||
      month == 10
    ) {
      if (date == 31) {
        ngayMai = 1;
        thangSau = month + 1;
        namSau = year;
        document.getElementById(
          "result-b1"
        ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
      } else {
        ngayMai = date + 1;
        thangSau = month;
        namSau = year;
        document.getElementById(
          "result-b1"
        ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
      }
    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
      if (date <= 30) {
        if (date == 30) {
          ngayMai = 1;
          thangSau = month + 1;
          namSau = year;
          document.getElementById(
            "result-b1"
          ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
        } else {
          ngayMai = date + 1;
          thangSau = month;
          namSau = year;
          document.getElementById(
            "result-b1"
          ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
        }
      } else {
        document.getElementById("result-b1").innerText = `Dữ liệu không hợp lệ`;
      }
    } else if (month == 2) {
      if (year % 4 == 0 && year % 100 != 0) {
        if (date <= 29) {
          if (date == 29) {
            ngayMai = 1;
            thangSau = month + 1;
            namSau = year;
            document.getElementById(
              "result-b1"
            ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
          } else {
            ngayMai = date + 1;
            thangSau = month;
            namSau = year;
            document.getElementById(
              "result-b1"
            ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
          }
        } else {
          document.getElementById(
            "result-b1"
          ).innerText = `Dữ liệu không hợp lệ`;
        }
      } else {
        if (date <= 28) {
          if (date == 28) {
            ngayMai = 1;
            thangSau = month + 1;
            namSau = year;
            document.getElementById(
              "result-b1"
            ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
          } else {
            ngayMai = date + 1;
            thangSau = month;
            namSau = year;
            document.getElementById(
              "result-b1"
            ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
          }
        } else if (date == 29) {
          document.getElementById(
            "result-b1"
          ).innerText = `Năm ${year} không phải là năm nhuận nên tháng 2 không có ngày 29 nhé ^^`;
        } else {
          document.getElementById(
            "result-b1"
          ).innerText = `Dữ liệu không hợp lệ`;
        }
      }
    } else {
      if (date == 31) {
        ngayMai = 1;
        thangSau = 1;
        namSau = year + 1;
        document.getElementById(
          "result-b1"
        ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
      } else {
        ngayMai = date + 1;
        thangSau = month;
        namSau = year;
        document.getElementById(
          "result-b1"
        ).innerText = `${ngayMai}/${thangSau}/${namSau}`;
      }
    }
  }
}
/**
 *
 * Input: nhập ngày, tháng, năm
 *
 * Các bước thực hiện:
 * - Ngày, tháng, năm của ngày tiếp theo:
 *    Bước1: Khai báo và gán biến date, month ,year theo giá trị của HTML element
 *    Bước2: Khai báo biến ngayHomQua, thangTruoc, namTruoc
 *    Bước3: Dùng cấu trúc if-else:
 *        Nếu ngày nhập vào bằng 1 thì:
 *           Nếu tháng nhập vào bằng 5 hoặc 7 hoặc 10 hoặc 12 thì ngayHomQua = 30; thangTruoc = month - 1; namTruoc = year;
 *           Nếu tháng nhập vào bằng 2 hoặc 4 hoặc 6 hoặc 8 hoặc 9 hoặc 11 thì ngayHomQua = 31; thangTruoc = month - 1; namTruoc = year;
 *           Nếu tháng nhập vào bằng 1 thì ngayHomQua = 31; thangTruoc = 12; namTruoc = year - 1;
 *           Trường hợp còn lại:
 *              Nếu năm nhuận thì ngayHomQua = 29; thangTruoc = month - 1; namTruoc = year;
 *              Nếu năm không nhuận thì ngayHomQua = 28; thangTruoc = month - 1; namTruoc = year;
 *        Trường hợp còn lại: ngayHomQua = date - 1; thangTruoc = month; namTruoc = year;
 *    Bước4: In kết quả ra giao diện web
 *
 * - Ngày tháng năm của ngày trước đó:
 *    Bước1: Khai báo và gán biến date, month ,year theo giá trị của HTML element
 *    Bước2: Khai báo biến ngayMai, thangSau, namSau
 *    Bước3: Dùng cấu trúc if-else:
 *        Nếu tháng nhập vào bằng 1 hoặc 3 hoặc 5 hoặc 7 hoặc 8 hoặc 10 thì:
 *           Nếu ngày nhập vào bằng 31 thì ngayMai = 1; thangSau = month + 1; namSau = year và in kết quả ra giao diện web
 *           Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *        Nếu tháng nhập vào bằng 4 hoặc 6 hoặc 9 hoặc 11 thì:
 *           Nếu ngày nhập vào bằng nhỏ hơn hoặc bằng 30 thì:
 *              Nếu ngày bằng 30 thì: ngayMai = 1; thangSau = month + 1; namSau = year và in kết quả ra giao diện web
 *              Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *           Trường hợp còn lại: in dòng chữ "Dữ liệu không hợp lệ"
 *        Nếu tháng nhập vào bằng 2 thì:
 *           Nếu năm nhập vào là năm nhuận thì:
 *              Nếu ngày bằng 29 thì: ngayMai = 1; thangSau = month + 1; namSau = year và in kết quả ra giao diện web
 *              Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *           Nếu năm nhập vào không là năm nhuận thì:
 *              Nếu ngày bằng nhỏ hơn hoặc bằng 28 thì:
 *                 Nếu ngày bằng 28 thì ngayMai = 1; thangSau = month + 1; namSau = year và in kết quả ra giao diện web
 *                 Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *              Trường hợp còn lại: in dòng chữ "Dữ liệu không hợp lệ"
 *        Trường hợp còn lại:
 *              Nếu ngày bằng 31 thì ngayMai = 1; thangSau = 1; namSau = year + 1 và in kết quả ra giao diện web
 *              Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *
 * Output: ngày, tháng, năm của ngày tiếp theo; ngày tháng năm của ngày trước đó
 */
